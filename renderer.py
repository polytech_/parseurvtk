import vtkmodules.all as vtk
import tkinter as tk
from tkinter.filedialog import askopenfilename

def OpenFile():
    '''
    Ouvre une fenêtre de dialogue pour selectionner un fichier
    :return: le chemin vers le fichier
    '''
    root = tk.Tk()
    name = askopenfilename(filetypes =[("VTK files", "*.vtk")],title = "Choisir un fichier vtk")
    root.destroy()
    return name

def RenderVTKPolydata(fileIn):
    '''
    Affiche un fichier de type polydata
    '''
    if (fileIn==""):
        print("Empty file name")
        return
    reader = vtk.vtkGenericDataObjectReader()
    reader.SetFileName(fileIn)
    reader.Update()

    polydata: vtk.vtkPolyData = reader.GetOutput()

    # Visualize
    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(polydata)
    else:
        mapper.SetInputData(polydata)

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)
    renderer.AddActor(actor)

    renderWindow.Render()
    renderWindowInteractor.Start()

if __name__ == '__main__':
    fileIn = OpenFile()
    RenderVTKPolydata(fileIn)

