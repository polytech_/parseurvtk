
def parseSection(sectionname, filepath):
    '''
    Parse la section dans le fichier
    :param filepath: le chemin vers le fichier
    :param sectionname: le nom da section (souvent en majuscule dans les fichiers)
    :return: le dictionnaire contenant les éléments {index, liste de strings}, le nombre d'éléments trouvés
    '''
    file1 = open(filepath, 'r')
    lines = file1.readlines()
    linedict: dict = {}  # dictionnaire, les clefs sont les index des éléments
    index = 0
    inSection = False
    for line in lines: # pour chaque ligne du fichier

        # ---> test si on est rentré dans la section
        if not inSection:
            issection = isSection(sectionname, line)
            if issection:
                inSection = True
            continue
        # test si on est rentré dans la section <---

        # ---> en train de parcourir la section
        if inSection:
            if not line or line.isspace():  # fin de la section
                break
            elements = line.split()
            linedict[index] = elements # ajoute les élements au dictionnaire
            index += 1
        # en train de parcourir la section <---

    return linedict, index

def isSection(sectionName, line):
    '''
    :param line: la ligne à tester
    :return: Booléen (True si est une section), Le nombre d'éléments dans la section
    '''
    elements = line.split()
    if len(elements) == 0 or elements[0] != sectionName:
        return False
    else:
        return True

#############################################################
#                   Script exemple
#############################################################
if __name__ == '__main__':
    import tkinter as tk
    from tkinter.filedialog import askopenfilename

    # ---> ouverture fichier
    root = tk.Tk()
    name = askopenfilename(filetypes=[("VTK files", "*.vtk")], title="Choisir un fichier vtk")
    root.destroy()
    # ouverture fichier <---

    lineDict, numelements = parseSection("LINES", name)
    for key in lineDict :
        print("key=" + str(key) + "  value="+str(lineDict[key]))
    print("------------------")
    print("Nombre d'elements=" + str(numelements))