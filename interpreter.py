import vtkparser


class Noeud:
    """
    représentation mémoire d'un noeud du graph
    """
    id: int = 0
    x: str = 0.0
    y: str = 0.0
    z: str = 0.0
    rayon: str = 0.0
    label: str = ""
    idregion: int = 0
    aliases = []  # les ids (numéro de ligne) du noeud dans le fichier vtk

    def __init__(self):
        self.aliases = []

    def isNoeud(self, alias: str):
        """
        Teste si l'alias donné correspond à ce noeud
        """
        if alias in self.aliases:
            return True
        else:
            return False

    def isNoeudCoord(self, x: str, y: str, z: str):
        """
        Teste si les coord données correspondent à ce noeud
        """
        if self.x == x and self.y == y and self.z == z:
            return True
        else:
            return False

    def __str__(self):
        return "[id=" + str(self.id) + \
               "][x=" + str(self.x) + \
               "][y=" + str(self.y) + \
               "][z=" + str(self.z) + \
               "][rayon=" + str(self.rayon) + \
               "][label=" + str(self.label) + \
               "][region=" + str(self.idregion) + \
               "][aliases=" + str(self.aliases) + \
               "]";


class Lien:
    noeudid1: int
    noeudid2: int
    label: str = ""


class Graph:
    noeuds: dict = {}
    liens: [Lien] = []

    def addRayonsToPoints(self, rayons: dict, points: dict):
        i = 0
        for key in rayons:  # les "radius sont rangés à la chaine en lignes de 3 maximum ..."
            elements = rayons[key]
            for element in elements:
                points[i].append(element)
                i += 1
        print("Taille rayons =" + str(i))

    def addNoeud(self, x: str, y: str, z: str, rayon: str, alias: str):
        '''
        Ajoute un noeud et/ou son alias à la liste des noeuds
        :return: True si il existait deja (juste l'alias a été ajouté)
        '''
        for key in self.noeuds:
            noeud = self.noeuds[key]
            if noeud.isNoeudCoord(x, y, z):
                noeud.aliases.append(alias)
                if noeud.rayon != rayon:
                    print("Attention : Même noeud, rayons différents (" + rayon + " et " + noeud.rayon + ")")
                return True
        newnoeud: Noeud = Noeud()
        newnoeud.id = len(self.noeuds)
        newnoeud.x = x
        newnoeud.y = y
        newnoeud.z = z
        newnoeud.rayon = rayon
        newnoeud.aliases.append(alias)
        newnoeud.idregion = 0  # ! insérer le code pour exploiter l'image volumique  !
        newnoeud.label = ""

        self.noeuds[newnoeud.id] = newnoeud
        return False

    def addLien(self, aliasNoeud1: str, aliasNoeud2: str):
        """
        Ajoute un lien à la liste des liens
        :return: False si l'un des noeuds n'existe pas
        """
        id1: int = -1
        id2: int = -1
        for key in self.noeuds:
            noeud = self.noeuds[key]
            if aliasNoeud1 in noeud.aliases:
                id1 = noeud.id
            if aliasNoeud2 in noeud.aliases:
                id2 = noeud.id
        if id1 == -1 or id2 == -1:
            return False
        else:
            lien = Lien()
            lien.noeudid1 = id1
            lien.noeudid2 = id2
            lien.label = ""
            self.liens.append(lien)

            return True

    def generateGraph(self, filename):
        print("Parsing des points ...")
        points, numpoints = vtkparser.parseSection("POINTS", filename)  # parse les noeuds
        print("Nombre de lignes trouvées = " + str(len(points)))
        print("Parsing des lignes ...")
        lines, numlines = vtkparser.parseSection("LINES", filename)
        print("Nombre de lignes trouvées = " + str(len(lines)))
        print("Parsing des rayons ...")
        rayons, rayonsnum = vtkparser.parseSection("Radius", filename)
        print("Ajout des rayons aux noeuds ...")
        self.addRayonsToPoints(rayons, points)
        print("Génération des noeuds ...")
        success, message = self.generateListeNoeuds(points)
        if not success:
            return False, message
        print("Génération des liens ...")
        success, message = self.generateListeLiens(lines)
        if not success:
            return False, message

        return True, "Ok"

    def generateListeLiens(self, lines: dict):
        for key in lines:  # pour chaque "ligne"
            elements = lines[key]
            for element in elements:  # vérifie que tout est numérique
                if not self.isFloat(element): return False, "Problème de format du lien (pas numérique) " + str(
                    elements)
            del elements[0]  # enleve le premier element car c'est juste le nombre d'indices qui vont suivre
            for i in range(len(elements) - 1):  # exemple : si (7, 8, 9, 10) : ajoute les liens (7,8),(8,9),(9,10)
                success = self.addLien(elements[i], elements[i + 1])
                if not success: return False, "Problème de format du lien (l'un des noeuds n'existe pas) : " + str(elements[i]) + "-" + str(elements[i + 1])
        return True, ""

    def generateListeNoeuds(self, points: dict):
        """
        génère la liste des noeuds et leurs alias
        :param points: le dictionnaire des points (trois floats sous forme de string)
        :return: True si a réussit
        """
        for key in points:
            elements = points[key]
            if len(elements) != 4: return False, "taille invalide dans noeud " + str(elements)  # (x,y,z,rayon)
            for element in elements:
                if not self.isFloat(element): return False, "un élement" + " [" + str(element) + "]" + " n'est pas numérique dans noeud " + str(elements)
            self.addNoeud(elements[0], elements[1], elements[2], elements[3], str(key))
        return True, ""

    def isFloat(self, str):
        try:
            float(str)
            return True
        except ValueError:
            return False


def writeGraphToFile(graph: Graph):
    filearetes = open("links.csv", "w")
    filearetes.close()
    filearetes = open("links.csv", "a")
    index_lien = 0
    for lien in graph.liens:
        filearetes.write(str(index_lien) + ";" + str(lien.noeudid1) + ";" + str(lien.noeudid2) + ";" + "\"" + str(lien.label) + "\"" + "\n")
        index_lien+=1
    filearetes.close()

    filenodes = open("nodes.csv", "w")
    filenodes.close()
    filenodes = open("nodes.csv", "a")
    filenodes.write("#id;x;y;z;rayon;region;label\n")  # en tête
    for key in graph.noeuds:
        noeud = graph.noeuds[key]
        filenodes.write(str(noeud.id) + ";" +
                        str(noeud.x) + ";" +
                        str(noeud.y) + ";" +
                        str(noeud.z) + ";" +
                        str(noeud.rayon) + ";" +
                        str(noeud.idregion) + ";" +
                        "\"" + str(noeud.label) + "\"" + "\n")
    filenodes.close()


if __name__ == '__main__':
    import tkinter as tk
    from tkinter.filedialog import askopenfilename

    # ---> ouverture fichier
    root = tk.Tk()
    filename = askopenfilename(filetypes=[("VTK files", "*.vtk")], title="Choisir un fichier vtk")
    root.destroy()
    # ouverture fichier <---

    graph: Graph = Graph()
    success, message = graph.generateGraph(filename)

    print(message)

    print("nombre de noeuds=" + str(len(graph.noeuds)) + " nombre de liens=" + str(len(graph.liens)))
    writeGraphToFile(graph)
