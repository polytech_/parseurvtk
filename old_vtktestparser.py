import vtkmodules.all as vtk
import csv

fileIn  = 'part_2172_network.vtk'
fileOut  = 'vtk_out.csv'

reader = vtk.vtkGenericDataObjectReader()
reader.SetFileName(fileIn)
reader.Update()

point_obj = reader.GetOutput()
points = point_obj.GetPoints()

table = vtk.vtkDataObjectToTable()
table.SetInputData(point_obj)
table.Update()
table.GetOutput().AddColumn(points.GetData())
table.Update()

writer = vtk.vtkDelimitedTextWriter()
writer.SetInputConnection(table.GetOutputPort())
writer.SetFileName(fileOut)
writer.Update()
writer.Write()