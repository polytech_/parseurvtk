from vtkmodules.all import vtkPolyDataReader
import vtkmodules.all as vtk

if __name__ == '__main__':
    reader = vtkPolyDataReader()
    reader.SetFileName("part_2172_network.vtk")
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()

    data: vtk.vtkPolyData = reader.GetOutput()
    pointData: vtk.vtkPointData = data.GetPointData()
    celldata: vtk.vtkCellData = data.GetCellData()

    array: vtk.vtkAbstractArray = pointData.GetAbstractArray(0)
    for i in range(array.GetNumberOfValues()):
        value = array.Get

    print("Polydata = " + str(reader.IsFilePolyData()))


